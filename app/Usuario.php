<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';

    public $timestamps = false;

    protected $attributes = [
        'rol_id' => false,
    ];

    protected $fillable = ['nombre, rol_id, empresa_id'];

    public function Rol(){
        return $this->belongsTo('App\Rol','rol_id','id');
    }

    public function Empresa(){
        return $this->belongsTo('App\Empresa','empresa_id','id')->withDefault();
    }

}
