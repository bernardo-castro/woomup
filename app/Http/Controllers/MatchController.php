<?php

namespace App\Http\Controllers;

use App\Match;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatchController extends Controller
{
    public function index() {
        $usuarios = Usuario::all();

        return view('pages.match',
            compact(
                'usuarios'
            )
        );
    }

    public function get($id) {
        $id = filter_var($id, FILTER_SANITIZE_STRING);

        //se cargan los match del usuario actual
        $currentMatches = DB::table('matches')
            ->where('usuario_id_1', '=',$id)
            ->orWhere('usuario_id_2', '=', $id)
            ->select(
                DB::raw('case  
                    when usuario_id_1 = "'.$id.'" then usuario_id_2
                    else usuario_id_1
                end as   usuario_id')
            )
            ->get();

        $matches = [];
        foreach ($currentMatches as $currentMatch) {
            array_push($matches, $currentMatch->usuario_id);
        }
        //se cargan los usuarios que no ha realizado match
        $usuariosMatch = DB::table('usuarios')
            ->select(
                'usuarios.id as usuario_id'
                , 'usuarios.nombre as usuario_nombre'
                , 'roles.nombre as rol_nombre'
                , 'empresas.nombre as empresa_nombre'
            )
            ->join('roles', 'roles.id', '=', 'usuarios.rol_id')
            ->leftJoin('empresas', 'empresas.id', '=', 'usuarios.empresa_id')
            ->whereIn('usuarios.id', $matches)
            ->get();

        array_push($matches, $id);

        //se valida que las mentoras no hagan match entre si
        $usuario = Usuario::find($id);
        $validacion = "1=1";
        if($usuario->rol->nombre == "Mentora") {
            $validacion = "roles.nombre <> 'Mentora'";
        }

        //
        $usuarios = DB::table('usuarios')
            ->select(
            'usuarios.id as usuario_id'
                , 'usuarios.nombre as usuario_nombre'
                , 'roles.nombre as rol_nombre'
                , 'empresas.nombre as empresa_nombre'
            )
            ->join('roles', 'roles.id', '=', 'usuarios.rol_id')
            ->leftJoin('empresas', 'empresas.id', '=', 'usuarios.empresa_id')
            ->whereNotIn('usuarios.id', $matches)
            ->whereRaw($validacion)
            ->get();

        if (count($usuarios)>0) {
            return response()
                ->json(['success' => true, 'data' => $usuarios, 'match'=>$usuariosMatch]);
        } else {
            return response()
                ->json(['success' => false, 'match'=>$usuariosMatch]);
        }
    }

    public function store(Request $request) {
        try{
            $match = new Match();
            $match->usuario_id_1 = $request->usuario_maestro;
            $match->usuario_id_2 = $request->usuario_match;
            $match->save();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }

    public function destroy(Request $request) {
        try{
            $match = DB::table('matches')
                ->whereRaw('(usuario_id_1 = ? and usuario_id_2 = ?)
                            or (usuario_id_1 = ? and usuario_id_2 = ?)'
                    , [
                        $request->usuario_maestro
                        , $request->usuario_match
                        , $request->usuario_match
                        , $request->usuario_maestro
                    ]
                );
            $match->delete();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }

}
