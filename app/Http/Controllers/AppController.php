<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Rol;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AppController extends Controller
{

    public function index() {
        $data = [];
        $usuarios = Usuario::all();

        foreach ($usuarios as $usuario) {
            $matches = DB::table('matches')
                ->where('usuario_id_1', '=', $usuario->id)
                ->orWhere('usuario_id_2', '=', $usuario->id)
                ->select(
                    \DB::raw('case  
                        when usuario_id_1 = "' . $usuario->id . '" then usuario_id_2
                        else usuario_id_1
                    end as   usuario_id')
                )
                ->get();

            $_matches = [];
            foreach ($matches as $value) {
                $_usuario = Usuario::find($value->usuario_id);
                $tipo = "";

                if($usuario->rol->nombre == 'Guiada'
                    && $_usuario->rol->nombre == 'Guiada'
                ) {
                    if($usuario->empresa->nombre <> $_usuario->empresa->nombre
                        || is_null($_usuario->empresa->nombre)
                        || is_null($usuario->empresa->nombre)
                    ){
                        $tipo = "Match de networking";
                    } else if($usuario->empresa->nombre == $_usuario->empresa->nombre
                        || !is_null($_usuario->empresa->nombre)
                        || !is_null($usuario->empresa->nombre)
                    ){
                        $tipo = "Match interno de empresa";
                    }
                } else if(
                    ($usuario->rol->nombre == 'Mentora' && $_usuario->rol->nombre == 'Guiada')
                    || ($_usuario->rol->nombre == 'Mentora' && $usuario->rol->nombre == 'Guiada')
                ) {
                    if($usuario->empresa->nombre <> $_usuario->empresa->nombre
                        || is_null($_usuario->empresa->nombre)
                        || is_null($usuario->empresa->nombre)
                    ){
                        $tipo = "Mentoría";
                    } else if($usuario->empresa->nombre == $_usuario->empresa->nombre
                        || !is_null($_usuario->empresa->nombre)
                        || !is_null($usuario->empresa->nombre)
                    ){
                        $tipo = "Mentoría interna de empresa";
                    }
                }

                $_matches[] = array(
                    "usuario_nombre" => $_usuario->nombre
                    , "usuario_empresa" => $_usuario->empresa->nombre
                    , "usuario_rol" => $_usuario->rol->nombre
                    , "tipo" => $tipo
                );
            }

            $data[] = array(
                "usuario" => array(
                    "nombre" => $usuario->nombre
                    , "empresa" => $usuario->empresa->nombre
                    , "rol" => $usuario->rol->nombre
                )
            , "matches" => $_matches
            );
        }

        return view('pages.home',
            compact(
                'data'
            )
        );
    }
}
