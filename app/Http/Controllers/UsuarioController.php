<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Rol;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    //
    public function index() {
        $usuarios = Usuario::orderBy('nombre','asc')->get();
        $roles = Rol::all();
        $empresas = Empresa::all();

        return view('pages.usuario',
            compact(
                'usuarios',
                'roles',
                'empresas'
            )
        );
    }

    public function store(Request $request) {
        try{
            $usuario = new Usuario();
            $usuario->nombre = $request->nombre;
            $usuario->rol_id = $request->rol;
            $usuario->empresa_id = $request->empresa;
            $usuario->save();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }

    public function update(Request $request) {
        try{
            $usuario = Usuario::find($request->id);

            $usuario->nombre = $request->nombre;
            $usuario->rol_id = (int)$request->rol;
            $usuario->empresa_id = (int)$request->empresa;
            $usuario->save();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }

    public function destroy(Request $request) {
        try{
            $usuario = Usuario::find($request->id);

            DB::table('matches')
                ->where('usuario_id_1', '=',$request->id)
                ->orWhere('usuario_id_2', '=', $request->id)
                ->delete();



            $usuario->delete();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }
}
