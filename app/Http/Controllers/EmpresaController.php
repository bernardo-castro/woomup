<?php

namespace App\Http\Controllers;

use App\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    //

    public function index() {

        $empresas = Empresa::orderBy('nombre','asc')->get();

        return view('pages.empresa',
            compact(
                'empresas'
            )
        );
    }

    public function store(Request $request) {
        try{
            $empresa = new Empresa();
            $empresa->nombre = $request->nombre;
            $empresa->save();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }

    public function update(Request $request) {
        try{
            $empresa = Empresa::find($request->id);
            $empresa->update(
                ['nombre' => $request->nombre]
            );
            $empresa->save();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }

    public function destroy(Request $request) {
        try{
            $empresa = Empresa::find($request->id);
            $empresa->delete();

            return response()
                ->json(['success' => true]);
        }
        catch(\Exception $e){
            return response()
                ->json(['success' => $e->getMessage()]);
        }
    }
}

