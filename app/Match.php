<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = 'matches';
    public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;

    //const CREATED_AT = 'fecha_creacion';
    //const UPDATED_AT = 'fecha_actualizacion';

    protected $fillable = ['usuario_id_1, usuario_id_2'];
}
