@extends('layouts.default')
@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Match's</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Match</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 30%">Usuario actual</th>
                            <th style="width: 60%">Busca tu Match</th>
                            <th style="width: 10%"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="input-group col-md-10">
                                        <select class="form-control pull-right row b-none usuario_maestro">
                                            <?php
                                            foreach($usuarios as $usuario) {
                                            ?>
                                            <option value="{{$usuario->id}}">{{$usuario->rol->nombre}} {{$usuario->nombre}} - {{($usuario->empresa->nombre)?$usuario->empresa->nombre:'Sin empresa'}}</option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group col-md-6">
                                        <select class="form-control pull-right row b-none usuario_match">
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-woomup" id="match">
                                        <i class="fa fa-group fa-fw" aria-hidden="true"></i>Match
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Listado de Match del usuario <label id="usuario_actual"></label></h3>
                <div class="table-responsive">
                    <table class="table table_match">
                        <thead>
                        <tr>
                            <th style="width: 30%">Rol</th>
                            <th style="width: 30%">Nombre</th>
                            <th style="width: 30%">Empresa</th>
                            <th style="width: 30%"><i class="fa fa-gear fa-fw" aria-hidden="true"></i>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div id="errorModal" class="modal fade" style="display: none;">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        Información
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body text-center">
                    <h4>No hay más Usuarios.</h4>
                    <p>El usuario actual ha hecho match's con todos.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="deleteModal" class="modal fade" style="display: none;" data-usuario-maestro="" data-usuario-match="">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">¿Estás seguro?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>¿Realmente quieres borrar este registro? Este proceso no se puede deshacer.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-woomup borrar">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(window).on("load",function(){
            $('.usuario_maestro').trigger('change');
        });
        $('.usuario_maestro').on('change', function(){
            var id = $(this).val();

            $.ajax({
                type:'GET',
                url:'/match/'+id,
                data:{
                    "_token": "{{ csrf_token() }}",
                },
                success:function(res, textStatus, xhr) {
                    $('.usuario_match').empty();
                    if(res.success == true) {
                        $('#match').removeClass('disabled');
                        res.data.forEach(function(value) {
                            var empresa = (value.empresa_nombre)?value.empresa_nombre:'Sin empresa';
                            $('.usuario_match').append($('<option>', {
                                    value:value.usuario_id
                                    , text:value.rol_nombre +' '+ value.usuario_nombre + ' - ' + empresa
                                }
                            ));
                        });
                    } else {
                        $('#errorModal').modal('show');
                        $('#match').addClass('disabled');
                    }

                    $('.table_match tbody').empty();
                    res.match.forEach(function(value){
                        var empresa_nombre = (value.empresa_nombre)?value.empresa_nombre:'Sin empresa';
                        $('.table_match tbody').append(
                            '<tr>\n' +
                            '    <td>'+value.rol_nombre +'</td>\n' +
                            '    <td>'+value.usuario_nombre +'</td>\n' +
                            '    <td>'+ empresa_nombre +'</td>\n' +
                            '    <td>\n' +
                            '        <button onclick="eliminar('+value.usuario_id +');"  type="button" class="btn btn-woomup-secondary eliminar">\n' +
                            '            <i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>\n' +
                            '            Eliminar match\n' +
                            '        </button>\n' +
                            '    </td>\n' +
                            '</tr>'
                        );
                    });
                }
            });
        });

        $('#match').on('click', function() {
            if($(this).attr("class").split(" ").indexOf("disabled") > 0) {
                return;
            }
            var usuario_maestro = $('.usuario_maestro').val();
            var usuario_match = $('.usuario_match').val();

            $.ajax({
                type:'POST',
                url:'/match',
                data:{
                    "_token": "{{ csrf_token() }}",
                    usuario_maestro: usuario_maestro,
                    usuario_match: usuario_match,
                },
                success:function(res, textStatus, xhr) {
                    if(res.success == true) {
                        location.reload();
                    } else {
                        console.log(res.success);
                        $('#errorModal').modal('show');
                    }
                }
            });
        });

        function eliminar(usuario_match) {
            $('#deleteModal').modal('show');
            $('#deleteModal').data('usuario-maestro', $('.usuario_maestro').val());
            $('#deleteModal').data('usuario-match', usuario_match);
        }
        $('.borrar').on('click', function(){
            var usuario_maestro = $('#deleteModal').data('usuario-maestro');
            var usuario_match = $('#deleteModal').data('usuario-match');

            $.ajax({
                type:'DELETE',
                url:'/match',
                data:{
                    "_token": "{{ csrf_token() }}",
                    usuario_maestro: usuario_maestro,
                    usuario_match: usuario_match,
                },
                success:function(data, textStatus, xhr) {
                    if(data.success == true) {
                        location.reload();
                    } else {
                        console.log(data.success);
                        $('#deleteModal').modal('hide');
                        $('#errorModal').modal('show');
                    }
                }
            });
        });

    </script>
@stop
