@extends('layouts.default')
@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Usuarias</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li>Mantenedores > Usuarias</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="col-md-3 col-sm-3 col-xs-4 pull-right">
                    <button type="button" class="btn btn-woomup btn-block waves-effect waves-light" data-toggle="modal" data-target="#addModal">
                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i> Agregar Usuaria
                    </button>
                </div>

                <h3 class="box-title">Listado de Usuarios</h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            {{--<th style="width: 10%">#</th>--}}
                            <th style="width: 20%">Nombre</th>
                            <th style="width: 20%">Rol</th>
                            <th style="width: 20%">Empresa</th>
                            <th style="width: 30%"><i class="fa fa-gear fa-fw" aria-hidden="true"></i>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach ($usuarios as $usuario){
                            ?>
                            <tr>
                                {{--<td>{{$empresa->id}}</td>--}}
                                <td>{{$usuario->nombre}}</td>
                                <td>{{$usuario->rol->nombre}}</td>
                                <td>{{($usuario->empresa->nombre)?$usuario->empresa->nombre:'Sin empresa'}}</td>
                                <td>
                                    <button
                                            data-id="{{$usuario->id}}"
                                            data-nombre="{{$usuario->nombre}}"
                                            data-rol="{{$usuario->rol->id}}"
                                            data-empresa="{{$usuario->empresa->id}}"
                                            type="button" class="btn btn-woomup-primary editar">
                                        <i class="fa fa-edit fa-fw" aria-hidden="true"></i>
                                        Editar
                                    </button>
                                    <button data-id="{{$usuario->id}}" type="button" class="btn btn-woomup-secondary eliminar">
                                        <i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>
                                        Eliminar
                                    </button>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addModalLabel">Agregar Usuaria</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            Nombre
                        </div>
                        <div class="input-group col-md-6">
                            <input type="text" class="form-control" id="nombre" aria-label="Nombre Empresa">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            Rol
                        </div>
                        <div class="input-group col-md-6">
                            <select class="form-control pull-right row b-none rol">
                                <?php
                                foreach($roles as $rol) {
                                  ?>
                                    <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            Empresa
                        </div>
                        <div class="input-group col-md-6">
                            <select class="form-control pull-right row b-none empresa">
                                <option>Sin empresa</option>
                                <?php
                                foreach($empresas as $empresa) {
                                ?>
                                    <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-deafult" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-woomup disabled" id="guardar" >Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true" data-id="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateModalLabel">Actualizar Usuaria</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            Nombre
                        </div>
                        <div class="input-group col-md-6">
                            <input type="text" class="form-control" id="updateNombre" aria-label="Nombre Empresa">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            Rol
                        </div>
                        <div class="input-group col-md-6">
                            <select class="form-control pull-right row b-none updateRol">
                                <?php
                                foreach($roles as $rol) {
                                ?>
                                <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            Empresa
                        </div>
                        <div class="input-group col-md-6">
                            <select class="form-control pull-right row b-none updateEmpresa">
                                <option>Sin empresa</option>
                                <?php
                                foreach($empresas as $empresa) {
                                ?>
                                <option value="{{$empresa->id}}">{{$empresa->nombre}}</option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-woomup" id="actualizar" >Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    <div id="errorModal" class="modal fade" style="display: none;">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        Error
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body text-center">
                    <h4>Ooops!</h4>
                    <p>Ha ocurrido un error.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="deleteModal" class="modal fade" style="display: none;" data-id="">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">¿Estás seguro?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>¿Realmente quieres borrar este registro? Este proceso no se puede deshacer.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-woomup borrar">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#nombre').on('keyup', function() {
            validationUsuario();
        });
        $('.rol').on('change', function() {
            validationUsuario();
        });
        $('#guardar').on('click', function() {
            if($(this).attr("class").split(" ").indexOf("disabled") > 0) {
                return;
            }
            var nombre = $('#nombre').val();
            var rol = $('.rol').val();

            var empresa = ($('.empresa').val() != "Sin empresa")?$('.empresa').val():"";
            $.ajax({
                type:'POST',
                url:'/usuario',
                data:{
                    "_token": "{{ csrf_token() }}",
                    nombre: nombre,
                    rol: rol,
                    empresa: empresa,
                },
                success:function(data, textStatus, xhr) {
                    if(data.success == true) {
                        location.reload();
                    } else {
                        console.log(data.success);
                        $('#updateNombre').val('');
                        $('#updateModal').modal('hide');
                        $('#errorModal').modal('show');
                    }
                }
            });
        });

        $('.editar').on('click', function(){
            $('#updateModal').modal('show');
            $('#updateModal').data('id', $(this).data('id'));

            $('#updateNombre').val($(this).data('nombre'));
            $('.updateRol').val($(this).data('rol'));
            if($(this).data('empresa')) {
                $('.updateEmpresa').val($(this).data('empresa'));
            } else {
                $('.updateEmpresa').val('Sin empresa');
            }
        });

        $('#updateNombre').on('keyup', function() {
            validationUpdateUsuario();
        });
        $('.updateRol').on('change', function() {
            validationUpdateUsuario();
        });

        $('#actualizar').on('click', function() {
            if($(this).attr("class").split(" ").indexOf("disabled") > 0) {
                return;
            }
            var id = $('#updateModal').data('id');
            var nombre = $('#updateNombre').val();
            var rol = $('.updateRol').val();
            var empresa= $('.updateEmpresa').val();
            $.ajax({
                type:'PUT',
                url:'/usuario',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id,
                    nombre: nombre,
                    rol: rol,
                    empresa: empresa,
                },
                success:function(data, textStatus, xhr) {
                    if(data.success == true) {
                        location.reload();
                    } else {
                        console.log(data.success);
                        $('#updateNombre').val('');
                        $('#updateModal').modal('hide');
                        $('#errorModal').modal('show');
                    }
                }
            });
        });

        $('.eliminar').on('click', function(){
            $('#deleteModal').modal('show');
            $('#deleteModal').data('id', $(this).data('id'));
        });

        $('.borrar').on('click', function(){
            var id = $('#deleteModal').data('id');
            $.ajax({
                type:'DELETE',
                url:'/usuario',
                data:{
                    "_token": "{{ csrf_token() }}",
                    id: id,
                },
                success:function(data, textStatus, xhr) {
                    if(data.success == true) {
                        location.reload();
                    } else {
                        console.log(data.success);
                        $('#updateNombre').val('');
                        $('#updateModal').modal('hide');
                        $('#errorModal').modal('show');
                    }
                }
            });
        });

        function validationUsuario() {
            if($('#nombre').val() && $('.rol').val()){
                $('#guardar').removeClass('disabled');
            } else {
                $('#guardar').addClass('disabled');
            }
        }
        function validationUpdateUsuario() {
            if($('#updateNombre').val() && $('.updateRol').val()){
                $('#actualizar').removeClass('disabled');
            } else {
                $('#actualizar').addClass('disabled');
            }
        }

    </script>
@stop
