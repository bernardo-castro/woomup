@extends('layouts.default')
@section('content')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Lista de Usuarias</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Lista de Usuarias</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th style="width:10%"><i class="fa fa-table"></i></th>
                            <th style="width:30%">Usuaria</th>
                            <th style="width:30%">Cantidad de match's</th>
                            <th style="width:30%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($data as $key=>$row) {
                            $_empresa = ($row["usuario"]["empresa"])?$row["usuario"]["empresa"]:'Sin empresa';
                            ?>
                            <tr class="clickable default" data-toggle="collapse" id="row{{$key}}" data-target=".row{{$key}}">
                                <td class="detail"><?php if(count($row["matches"])){?> <i class="fa fa-angle-right"></i><?php } ?></td>
                                <td>{{$row["usuario"]["rol"]. ' ' . $row["usuario"]["nombre"] . ' - ' . $_empresa }}</td>
                                <td>{{count($row["matches"])}}</td>
                                <td></td>
                            </tr>

                            <?php
                                foreach ($row["matches"] as $match) {
                                $_empresa2 = ($match["usuario_empresa"])?$match["usuario_empresa"]:'Sin empresa';
                            ?>
                                <tr class="info collapse row{{$key}}">
                                    <td></td>
                                    <td></td>
                                    <td>{{$match["usuario_rol"].' '. $match["usuario_nombre"] . ' - ' . $_empresa2}}</td>
                                    <td>{{$match["tipo"]}}</td>
                                </tr>
                            <?php
                            }
                            ?>
                        <?php
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.clickable').on('click', function(){
            console.log($(this).find("i").attr("class"));
            if($(this).find("i").attr("class") == "fa fa-angle-right"){
                $(this).find("i").removeClass("fa-angle-right");
                $(this).find("i").addClass("fa-angle-down");
            } else {
                $(this).find("i").addClass("fa-angle-right");
                $(this).find("i").removeClass("fa-angle-down");
            }
        });
    </script>

@stop