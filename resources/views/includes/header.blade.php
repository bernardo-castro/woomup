<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part show-menu">
                <a class="logo" href="#">
                    <img src="https://cdn.woomup.cl/assets/themes/woomup/favicon.ico" alt="logo-img" width="36" class="img-rounded" style="background: white;">
                    <b class="hidden-xs">WoomUp</b>
                </a>
            </div>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li>
                    <a class="profile-pic" href="#">
                        <img src="{{asset('images/girl.svg')}}" alt="user-img" width="36" class="img-circle">
                        <b class="hidden-xs"></b>
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav slimscrollsidebar">
            <div class="sidebar-head">
                <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
            </div>
            <ul class="nav" id="side-menu">
                <li style="padding: 70px 0 0;">
                    <a href="{{ url('/') }}" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i>Listado de Usuarias</a>
                </li>
                <li>
                    <a href="{{url('match')}}" class="waves-effect"><i class="fa fa-group fa-fw" aria-hidden="true"></i>Busca tu Match's</a>
                </li>
                <li>
                    <a href="{{url('usuario')}}" class="waves-effect"><i class="fa fa-female fa-fw" aria-hidden="true"></i>Mantenedor de Usuarias</a>
                </li>
                <li>
                    <a href="{{url('empresa')}}" class="waves-effect"><i class="fa fa-cubes fa-fw" aria-hidden="true"></i>Mantenedor de Empresas</a>
                </li>
            </ul>
        </div>

    </div>