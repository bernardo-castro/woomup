
<script>
    $('.show-menu').on('click', function() {
        if($('body').attr('class').split(" ").indexOf("content-wrapper") > 0) {
            $('body').removeClass('content-wrapper');
        } else {
            $('body').addClass('content-wrapper');
        }
    });
</script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/sidebar-nav.min.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('js/waves.js') }}"></script>
<script src="{{ asset('js/jquery.waypoints.js') }}"></script>
<script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('js/custom.min.js') }}"></script>
<script src="{{ asset('js/jquery.toast.js') }}"></script>
