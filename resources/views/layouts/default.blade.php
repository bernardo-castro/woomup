<!DOCTYPE html>
<html lang="es">

<head>
@include('includes.head')
</head>
<body>
<body class="fix-header">
    @include('includes.header')

    <div id="page-wrapper">
        <div class="container-fluid">
            @yield('content')

            <footer class="footer text-center">
                @include('includes.footer')
            </footer>
        </div>
    </div>
    @include('includes.foot')
</body>
</html>