<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@index');
Route::get('/show/{id}', 'AppController@show');


Route::get('/match', 'MatchController@index');
Route::get('/match/{id}', 'MatchController@get');
Route::post('/match', 'MatchController@store');
Route::delete('/match', 'MatchController@destroy');

Route::get('/usuario', 'UsuarioController@index');
Route::post('/usuario', 'UsuarioController@store');
Route::put('/usuario', 'UsuarioController@update');
Route::delete('/usuario', 'UsuarioController@destroy');

Route::get('/empresa', 'EmpresaController@index');
Route::post('/empresa', 'EmpresaController@store');
Route::put('/empresa', 'EmpresaController@update');
Route::delete('/empresa', 'EmpresaController@destroy');

