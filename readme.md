WoomUp Aplicación Test
===

    
Para crear localmente seguir los siguientes pasos:

1. tener instalado xampp
2. Clonar el repositorio en la maquina local
3. Crear la base de datos llamada "woomup"
4. Importar datos desde la base de datos guardada en db/woomup.sql
5. Modificar el archivo httpd-vhosts.conf en la carpeta de "C:\xampp\apache\conf\extra" y agregar
    
        <VirtualHost *:80>
        #ServerAdmin webmaster@dummy-host.example.com
        DocumentRoot "C:/xampp/htdocs/woomup/public"
        ServerName local.woomup.cl
        #ErrorLog "C:/xampp/htdocs/woomup/storage/log/error_log.txt"
        </VirtualHost>
    
6. Agregar la linea en C:\Windows\System32\drivers\etc\hosts:

        127.0.0.1 local.woomup.cl

7. Reiniciar Apache
8. Acceder desde el navegador a local.woomup.cl
